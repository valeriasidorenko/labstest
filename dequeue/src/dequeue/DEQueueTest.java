package dequeue;

import static org.junit.Assert.*;

import org.junit.Test;

public class DEQueueTest {

	
	/*@Before
  public void init() {
	      array = new int[10];  }*/

	@Test
	public void testIsEmptyAfterCreate() { 
		DEQueue<String> dq = new DEQueue<>();
		assertEquals(0, dq.size());
	}
	
	@Test
	public void testPushBackOneString() { //��� pushback
		DEQueue<String> dq = new DEQueue<>();
		dq.pushBack("first");
		assertEquals(1, dq.size());
		assertEquals("first", dq.back());
	}
	
	@Test
	public void testPushFrontTwoString() { //pushfront
		DEQueue<String> dq = new DEQueue<>();
		dq.pushFront("one");
		dq.pushFront("two");
		assertEquals(2, dq.size());
		assertEquals("two", dq.front());
	}
	
	@Test
	public void testPopFront() { //popfront
		DEQueue<String> dq = new DEQueue<>();
		dq.pushFront("one");
		dq.pushFront("two");
		assertEquals("two", dq.popFront());
	}
	
	@Test
	public void testPopBack() { //poback
		DEQueue<String> dq = new DEQueue<>();
		dq.pushFront("mother");
		dq.pushFront("father");
		dq.pushFront("brother");
		assertEquals("mother", dq.popBack());
	}
	
	
	@Test
	public void testBack() { //back
		DEQueue<String> dq = new DEQueue<>();
		dq.pushBack("mother");
		dq.pushFront("father");
		assertEquals("mother", dq.back());
	}

	@Test
	public void testFront() { //front
		DEQueue<String> dq = new DEQueue<>();
		dq.pushBack("str");
		dq.pushBack("str1");
		assertEquals("str", dq.front());
	}
	
	@Test
	public void testForPeek() { //��� peek
		DEQueue<String> dq = new DEQueue<>();
		dq.pushBack("first");
		dq.pushBack("second");
		assertEquals("first", dq.peek(0));
	}
	
	@Test
	public void testForPeekNull() { //��� ������������ peek
		DEQueue<String> dq = new DEQueue<>();
		assertNull(dq.peek(4));
	
	}
	
	@Test
	public void testForClear() { //��� clear
		DEQueue<String> dq = new DEQueue<>();
		dq.pushBack("first");
		dq.pushBack("second");
		dq.clear();
		assertEquals(0, dq.size());
	}
	
	@Test
	public void testForToArray() { //��� toArray()
		DEQueue<String> dq = new DEQueue<>();
		dq.pushBack("first");
		dq.pushBack("second");
		System.out.println(dq.toArray()[0]);
		//assertEquals(2, dq.size());
	}
	
}

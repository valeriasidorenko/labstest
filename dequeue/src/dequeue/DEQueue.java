package dequeue;

import java.util.ArrayList;

public class DEQueue<T> {

	ArrayList<T> q = new ArrayList<T>();

	public DEQueue() {
		// �������� ����������� ���

	}

	public void pushBack(T element) {
		q.add(element);

	}

	public void pushFront(T element) {
		q.add(0, element);
	}

	public T popFront() {
		T perem = null;
		if (q.isEmpty()) {
			perem = null;
		} else {
			perem = q.get(0);
			q.remove(0);
		}
		return perem;

	}

	public T popBack() {
		T perem = null;
		if (q.isEmpty()) {
			perem = null;
		} else {
			perem = q.get(q.size() - 1);
			q.remove(q.size() - 1);
		}
		return perem;

	}

	public T back() {
		T perem = null;

		if (q.isEmpty()) {
			perem = null;
		} else {

			perem = q.get(q.size() - 1);
		}
		return perem;
	}

	public T front() {
		T perem = null;
		if (q.isEmpty()) {
			perem = null;
		} else {
			perem = q.get(0);
		}
		return perem;

	}

	public T peek(int position) {
		T perem = null;
		if (q.isEmpty()) {
			perem = null;
		} else {
			perem = q.get(position);
		}
		return perem;
	}

	public int size() {

		return q.size();
	}

	public void clear() {

		q.clear();
	}

	public T[] toArray() {

		T[] mas = (T[]) java.lang.reflect.Array.newInstance(q.get(0).getClass(), q.size());

		return q.toArray(mas);
	}

}
